Branch kezelés:
 - Master: Csak frontend
 - Master_with_backend: frontend + backend

Backend indítás:
 - java -jar /app_name/ -a jars mappában

Build:
 Mavennel van menedzselve a projekt, internetkapcsolat szükséges a függőségek letöltéséhez.

 mvn clean install - a gyökérmappában

Szükséges:
 java 1.8
 maven
 
