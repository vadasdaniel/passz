<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <title>Igyál app</title>
</head>
<body style="background: url(../img/bg-masthead.jpg) no-repeat center center; background-size: cover;">
    <div id="logreg-forms">
        <form action="login.php" class="form-signin" method="post">
            <h1 class="h3 mb-3 font-weight-normal" style="text-align: center">Bejelentkezés</h1>
            <div class="social-login">
                <button class="btn facebook-btn social-btn" type="button"><span><i class="fab fa-facebook-f"></i> Facebook-al</span> </button>
                <button class="btn google-btn social-btn" type="button"><span><i class="fab fa-google-plus-g"></i> Google+-al</span> </button>
            </div>
            <p style="text-align:center">vagy</p>
            <input type="email" id="inputEmail" name="email" class="form-control" placeholder="E-mail cím" required="" autofocus="">
            <input type="password" id="inputPassword" name="jelszo" class="form-control" placeholder="Jelszó" required="">
            <input type="hidden" name="hely" value="<?= $_POST["hely"] ?>">
            <button class="btn btn-success btn-block" type="submit"><i class="fas fa-sign-in-alt"></i> Bejelentkezés</button>
            <a href="#" id="forgot_pswd">Elfelejtette a jelszavát?</a>
            <hr>
            <button class="btn btn-primary btn-block" type="button" id="btn-signup"><i class="fas fa-user-plus"></i> Regisztráció</button>
            </form>
            <form action="" class="form-reset" method="post">
                <input type="email" id="resetEmail" class="form-control" placeholder="E-mail cím" required="" autofocus="">
                <button class="btn btn-primary btn-block" type="submit">Új jelszó kérése</button>
                <a href="#" id="cancel_reset"><i class="fas fa-angle-left"></i> Vissza</a>
            </form>
            <form action="register.php" class="form-signup" method="post">
            <h1 class="h3 mb-3 font-weight-normal" style="text-align: center">Regisztráció</h1>
                <div class="social-login">
                    <button class="btn facebook-btn social-btn" type="button"><span><i class="fab fa-facebook-f"></i> Facebook-al</span> </button>
                </div>
                <div class="social-login">
                    <button class="btn google-btn social-btn" type="button"><span><i class="fab fa-google-plus-g"></i> Google+-al</span> </button>
                </div>
                <p style="text-align:center">vagy</p>
                <input type="email" id="user-email" name="email" class="form-control" placeholder="E-mail cím" required autofocus="">
                <input type="password" id="user-pass" name="jelszo1" class="form-control" placeholder="Jelszó" required autofocus="">
                <input type="password" id="user-repeatpass" name="jelszo2" class="form-control" placeholder="Jelszó megerősítése" required autofocus="">
                <input type="hidden" name="hely" value="<?= $_POST["hely"] ?>">
                <button class="btn btn-primary btn-block" type="submit"><i class="fas fa-user-plus"></i> Regisztráció</button>
                <a href="#" id="cancel_signup"><i class="fas fa-angle-left"></i> Vissza</a>
            </form>
    </div>
    <style>
    #logreg-forms{
        width:412px;
        margin:10vh auto;
        background-color:#f3f3f3;
        box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        transition: all 0.3s cubic-bezier(.25,.8,.25,1);
    }
    #logreg-forms form {
        width: 100%;
        max-width: 410px;
        padding: 15px;
        margin: auto;
    }
    #logreg-forms .form-control {
        position: relative;
        box-sizing: border-box;
        height: auto;
        padding: 10px;
        font-size: 16px;
    }
    #logreg-forms .form-control:focus { z-index: 2; }
    #logreg-forms .form-signin input[type="email"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }
    #logreg-forms .form-signin input[type="password"] {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
    #logreg-forms .social-login{
        width:390px;
        margin:0 auto;
        margin-bottom: 14px;
    }
    #logreg-forms .social-btn{
        font-weight: 100;
        color:white;
        width:190px;
        font-size: 0.9rem;
    }
    #logreg-forms a{
        display: block;
        padding-top:10px;
        color:lightseagreen;
    }
    #logreg-form .lines{
        width:200px;
        border:1px solid red;
    }
    #logreg-forms button[type="submit"]{ margin-top:10px; }
    #logreg-forms .facebook-btn{  background-color:#3C589C; }
    #logreg-forms .google-btn{ background-color: #DF4B3B; }
    #logreg-forms .form-reset, #logreg-forms .form-signup{ display: none; }
    #logreg-forms .form-signup .social-btn{ width:210px; }
    #logreg-forms .form-signup input { margin-bottom: 2px;}
    .form-signup .social-login{
        width:210px !important;
        margin: 0 auto;
    }
    @media screen and (max-width:500px){
        #logreg-forms{
            width:300px;
        } 
        #logreg-forms  .social-login{
            width:200px;
            margin:0 auto;
            margin-bottom: 10px;
        }
        #logreg-forms  .social-btn{
            font-size: 1.3rem;
            font-weight: 100;
            color:white;
            width:200px;
            height: 56px;
        }
        #logreg-forms .social-btn:nth-child(1){
            margin-bottom: 5px;
        }
        #logreg-forms .social-btn span{
            display: none;
        }
        #logreg-forms  .facebook-btn:after{
            content:'Facebook';
        }
        #logreg-forms  .google-btn:after{
            content:'Google+';
        }
    }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script>
    function toggleResetPswd(e){
        e.preventDefault();
        $('#logreg-forms .form-signin').toggle()
        $('#logreg-forms .form-reset').toggle()
    }

    function toggleSignUp(e){
        e.preventDefault();
        $('#logreg-forms .form-signin').toggle();
        $('#logreg-forms .form-signup').toggle();
    }

    $(()=>{
        $('#logreg-forms #forgot_pswd').click(toggleResetPswd);
        $('#logreg-forms #cancel_reset').click(toggleResetPswd);
        $('#logreg-forms #btn-signup').click(toggleSignUp);
        $('#logreg-forms #cancel_signup').click(toggleSignUp);
    })
    </script>
</body>
</html>