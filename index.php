<!DOCTYPE html>
<html lang="hu">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Igyál app</title>
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
  <link href="css/landing-page.min.css" rel="stylesheet">
</head>
<body>
  <nav class="navbar navbar-light bg-light static-top">
    <div class="container">
      <a class="navbar-brand" href="#">Igyál app</a>
      <a class="btn btn-success" href="#">Bejelentkezés</a>
    </div>
  </nav>
  <header class="masthead text-center">
    <div class="container">
      <div class="row">
        <div class="col-xl-9 mx-auto">
          <h1 style="color: rgba(0,0,0,.9);" class="mb-5">Kocsmát keresel? Írd be a település nevét és kattints a keresés gombra!</h1>
        </div>
        <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
          <form action="bejelentkezes/" method="post">
            <div class="form-row">
              <div class="col-12 col-md-9 mb-2 mb-md-0">
                <input type="text" name="hely" class="form-control form-control-lg" placeholder="Település" required>
              </div>
              <div class="col-12 col-md-3">
                <button type="submit" class="btn btn-block btn-lg btn-success">Keresés</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </header>
  <footer class="footer bg-light">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
          <ul class="list-inline mb-2">
            <li class="list-inline-item">
              <a style="color: rgba(0,0,0,.9);" href="#">Rólunk</a>
            </li>
            <li class="list-inline-item">&sdot;</li>
            <li class="list-inline-item">
              <a style="color: rgba(0,0,0,.9);" href="#">Kapcsolat</a>
            </li>
            <li class="list-inline-item">&sdot;</li>
            <li class="list-inline-item">
              <a style="color: rgba(0,0,0,.9);" href="#">Adatvédelmi tájékoztató</a>
            </li>
          </ul>
          <p class="text-muted small mb-4 mb-lg-0">&copy; Copyright 2019 Igyal.hu - Minden jog fenntartva</p>
        </div>
        <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
          <ul class="list-inline mb-0">
            <li class="list-inline-item mr-3">
              <a href="#">
                <i style="color: rgba(0,0,0,.9);"class="fab fa-facebook fa-2x fa-fw"></i>
              </a>
            </li>
            <li class="list-inline-item mr-3">
              <a href="#">
                <i style="color: rgba(0,0,0,.9);" class="fab fa-twitter-square fa-2x fa-fw"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i style="color: rgba(0,0,0,.9);" class="fab fa-instagram fa-2x fa-fw"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>