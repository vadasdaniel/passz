<?php
session_start();
include("../bejelentkezes/adatbazis.php");
$email = $_SESSION["email"];
$hely = $_POST["hely"];
if($_SESSION["email"] == "" OR $_POST["hely"] == ""){
	header("Location: ../");
}
?>
<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <title>Igyál app</title>
</head>
<body>
    <div class="container-fluid">
      <div class="row" style="min-height:100vh;">
        <div class="col-sm-4 p-4">
        <?php
        $result = mysqli_query($db_connect,"SELECT * FROM pubs");
        while($row = mysqli_fetch_row($result)){
            $address = mysqli_query($db_connect,"SELECT * FROM address WHERE id=" . $row[2]);
            while($add = mysqli_fetch_row($address)){
                $place = $add[4] . "," . $add[5];
                echo '<div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">' . $row[1] . '</h5>
                        <p class="card-text">Cím: ' . $add[2] . '<br>Értékelés: ' . $row[3] . '<br>Nyitás: ' . $row[4] . '</p>
                        <a href="https://www.google.com/maps/dir//' . $place . '" class="btn btn-success" target="_blank">Itt szeretnék inni</a>
                        <a href="/app/kocsma/" class="btn btn-secondary" target="_blank">Több infó</a>
                    </div>
                </div>';
            }
        }
        ?>
        </div>
        <div class="col-sm-8">
            <iframe id="hely" src="" frameborder="0"></iframe>
        </div>
      </div>
    </div>
    <style>
    iframe{
        height:100%;
        width:85%;
        position:fixed;
    }
    .card{
        border: 1px solid #000;
    }
    body{
        background-color: #28a745;
    }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAJX5d37h9dCYatKbheaqKOuj_-RGgCLGU"></script>
    <script type="text/javascript">

    var geocoder = new google.maps.Geocoder();
    var address = '<?= $hely ?>';

    geocoder.geocode( { 'address': address}, function(results, status) {

    if (status == google.maps.GeocoderStatus.OK) {
        var latitude = results[0].geometry.location.lat();
        var longitude = results[0].geometry.location.lng();
        document.getElementById('hely').src = 'https://www.google.com/maps/embed/v1/search?key=AIzaSyAJX5d37h9dCYatKbheaqKOuj_-RGgCLGU&q=Kocsma+in+<?= $hely ?>';
        } 
    }); 
    </script>
</body>
</html>