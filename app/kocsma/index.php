<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <title>Igyál app</title>
</head>
<body>
    <main>

    <section class="jumbotron text-center">
        <div class="container">
        <h1>Gesztenyés Söröző</h1>
        <a href="https://www.google.com/maps/dir//47.0908647,17.9015938" class="btn btn-success" target="_blank">Itt szeretnék inni</a>
        <p>
            <a href="#" class="btn btn-primary my-2">Italok</a>
            <a href="#" class="btn btn-secondary my-2">Ételek</a>
        </p>
        </div>
    </section>

    <div class="album py-3">
        <div class="container">

        <div class="row">
            <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <div class="card-body">
                <p class="card-text">Sör (1 korsó): 400 Ft</p>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <div class="card-body">
                <p class="card-text">Bor (1 dl): 200 Ft</p>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <div class="card-body">
                <p class="card-text">Vodka (4 cl): 600 Ft</p>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <div class="card-body">
                <p class="card-text">Sör (1 pohár): 300 Ft</p>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <div class="card-body">
                <p class="card-text">Fröccs (2+1 dl): 400 Ft</p>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <div class="card-body">
                <p class="card-text">Pálinka (4 cl): 600 Ft</p>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <div class="card-body">
                <p class="card-text">Koktél 1 (3 dl): 800 Ft</p>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <div class="card-body">
                <p class="card-text">Koktél 2 (3 dl): 1000 Ft</p>
                </div>
            </div>
            </div>
            <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <div class="card-body">
                <p class="card-text">Koktél 3 (3 dl): 900 Ft</p>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>

    </main>
    <style>
    .card{
        border: 1px solid #000;
    }
    body{
        background-color: #28a745;
    }
    </style>
</body>
</html>