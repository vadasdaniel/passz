package hu.pe.sf.passz.restapi.controller;

import hu.pe.sf.passz.storage.user.UserEntity;
import hu.pe.sf.passz.storage.user.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/security")
public class SecurityController {

    Logger logger = LoggerFactory.getLogger(SecurityController.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    PasszUserDetailsService userDetailsService;

    @Autowired
    AuthenticationManager authenticationManager;

    @PostMapping("/registration")
    private void registration(@RequestParam("email") String email,
                              @RequestParam("password") String password) {

        UserEntity user = new UserEntity();
        user.setEmail(email);
        user.setPassword(bCryptPasswordEncoder.encode(password));

        if(userRepository.findByEmail(email) == null)
            userRepository.save(user);
    }

    @GetMapping("/login")
    private void login(@RequestParam("email") String email,
                       @RequestParam("password") String password) {

        UserEntity user = userRepository.findByEmail(email);
        if (bCryptPasswordEncoder.matches(password, user.getPassword())){
            autoLogin(email, password);
        }
    }

    public void autoLogin(String username, String password) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());

        authenticationManager.authenticate(usernamePasswordAuthenticationToken);

        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            logger.info(String.format("Auto login %s successfully!", username));
        }
    }

    public String findLoggedInUsername() {
        Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();
        if (userDetails instanceof UserDetails) {
            return ((UserDetails)userDetails).getUsername();
        }
        return null;
     }
}
