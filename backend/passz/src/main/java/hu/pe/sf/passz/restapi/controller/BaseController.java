package hu.pe.sf.passz.restapi.controller;

import hu.pe.sf.passz.storage.user.UserEntity;
import hu.pe.sf.passz.storage.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BaseController {

    @Autowired
    public UserRepository userRepository;

    @Autowired
    public BCryptPasswordEncoder bCryptPasswordEncoder;

    public String LoggedInUserName() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    public UserEntity loggedInUser() {
        return userRepository.findByEmail(LoggedInUserName());
    }
}
