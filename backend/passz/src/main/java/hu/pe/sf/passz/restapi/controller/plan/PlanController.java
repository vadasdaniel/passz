package hu.pe.sf.passz.restapi.controller.plan;

import hu.pe.sf.passz.restapi.controller.BaseController;
import hu.pe.sf.passz.storage.plan.PlanEntity;
import hu.pe.sf.passz.storage.plan.PlanRepository;
import hu.pe.sf.passz.storage.user.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/plans")
public class PlanController extends BaseController {

    @Autowired
    PlanRepository planRepository;

    @PutMapping("/create")
    private void create(@RequestBody PlanEntity planEntity){
        planRepository.save(planEntity);
    }

    @GetMapping("/myplans")
    @ResponseBody
    private List<PlanEntity> myplans() {
        UserEntity entity = loggedInUser();
        return planRepository.findByOwnerId(entity.getId());
    }

    @PostMapping("/modify")
    private void modify(@RequestBody PlanEntity entity) {
        planRepository.save(entity);
    }

    @DeleteMapping("/delete/{id}")
    private void delete(@PathVariable("id") Long id) {
        planRepository.deleteById(id);
    }

    @PostMapping("/{planId}/addmember/{userId}")
    private void addMember(@PathVariable("planId") Long planId,
                           @PathVariable("userId") Long userId) {
        PlanEntity entity = planRepository.getOne(planId);
        UserEntity userEntity = userRepository.findById(userId).get();
        List<UserEntity> memberList = entity.getMembers();
        memberList.add(userEntity);
        entity.setMembers(memberList);
    }

    @GetMapping("/list")
    @ResponseBody
    private List<PlanEntity> list(){
        return planRepository.findAll();
    }
}