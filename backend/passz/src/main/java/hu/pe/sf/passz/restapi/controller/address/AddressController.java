package hu.pe.sf.passz.restapi.controller.address;

import hu.pe.sf.passz.restapi.controller.BaseController;
import hu.pe.sf.passz.storage.address.AddressEntity;
import hu.pe.sf.passz.storage.address.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/address")
public class AddressController extends BaseController {

    @Autowired
    AddressRepository addressRepository;

    @PutMapping("/create")
    private void create(@RequestBody AddressEntity entity){
        addressRepository.save(entity);
    }

}
