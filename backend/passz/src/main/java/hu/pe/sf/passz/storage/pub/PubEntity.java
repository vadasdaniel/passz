package hu.pe.sf.passz.storage.pub;

import hu.pe.sf.passz.storage.address.AddressEntity;
import hu.pe.sf.passz.storage.drink.DrinkEntity;
import hu.pe.sf.passz.storage.food.FoodEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "pubs")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PubEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address", referencedColumnName = "id")
    private AddressEntity address;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "drink_list",
            joinColumns = @JoinColumn(name = "pub_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "drink_id", referencedColumnName = "id"))
    private List<DrinkEntity> drinks = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "food_list",
            joinColumns = @JoinColumn(name = "pub_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "food_id", referencedColumnName = "id"))
    private List<FoodEntity> foods = new ArrayList<>();


}
