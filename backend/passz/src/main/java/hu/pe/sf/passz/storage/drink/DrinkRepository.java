package hu.pe.sf.passz.storage.drink;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DrinkRepository extends JpaRepository<DrinkEntity, Long> {

    List<DrinkEntity> findAll();
}
