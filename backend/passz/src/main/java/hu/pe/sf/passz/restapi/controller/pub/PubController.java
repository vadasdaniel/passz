package hu.pe.sf.passz.restapi.controller.pub;

import hu.pe.sf.passz.storage.pub.PubEntity;
import hu.pe.sf.passz.storage.pub.PubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pubs")
public class PubController {

    @Autowired
    PubRepository pubRepository;

    @PostMapping("/create")
    private void create(@RequestBody PubEntity entity) {
        pubRepository.save(entity);
    }
}