package hu.pe.sf.passz.restapi.controller.food;

import hu.pe.sf.passz.restapi.controller.BaseController;
import hu.pe.sf.passz.storage.food.FoodEntity;
import hu.pe.sf.passz.storage.food.FoodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/food")
public class FoodController extends BaseController{

    @Autowired
    FoodRepository foodRepository;

    @PostMapping("/create")
    private void create(@RequestBody FoodEntity entity) {
        foodRepository.save(entity);
    }

    @GetMapping("/list")
    private List<FoodEntity> list(){
        return foodRepository.findAll();
    }


}