package hu.pe.sf.passz.restapi.controller.user;

import hu.pe.sf.passz.restapi.controller.BaseController;
import hu.pe.sf.passz.storage.user.UserEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController extends BaseController {

    @PostMapping("/modify")
    private void modify(@RequestBody UserEntity entity) {
        if (entity.getPassword() != null)
            entity.setPassword(bCryptPasswordEncoder.encode(entity.getPassword()));
        userRepository.save(entity);
    }

    @GetMapping("/me")
    @ResponseBody
    private UserEntity me(){
        UserEntity entity = loggedInUser();
        entity.setPassword(null);
        return entity;
    }
    
}