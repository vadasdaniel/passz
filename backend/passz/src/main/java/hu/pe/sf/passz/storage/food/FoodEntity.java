package hu.pe.sf.passz.storage.food;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "foods")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FoodEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private String price;

    @Column(name = "unit")
    private String unit;

    @Column(name = "allergens")
    private String allergens;
}
