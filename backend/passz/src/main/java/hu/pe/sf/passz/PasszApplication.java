package hu.pe.sf.passz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PasszApplication {

    public static void main(String[] args) {
        SpringApplication.run(PasszApplication.class, args);
    }

}
