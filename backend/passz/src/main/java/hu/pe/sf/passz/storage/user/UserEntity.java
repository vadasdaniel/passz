package hu.pe.sf.passz.storage.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "passwordConfirm")
    @Transient
    private String passwordConfirm;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "age")
    private int age;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "last_position")
    private String lastPosition;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "friend_list",
            joinColumns = @JoinColumn(name = "me_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "other_id", referencedColumnName = "id"))
    private List<UserEntity> friends = new ArrayList<>();

    @ManyToMany
    private Set<Role> roles;


}
