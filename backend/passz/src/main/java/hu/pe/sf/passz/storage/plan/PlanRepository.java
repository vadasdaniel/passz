package hu.pe.sf.passz.storage.plan;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlanRepository extends JpaRepository<PlanEntity, Long> {

    List<PlanEntity> findAll();

    List<PlanEntity> findByOwnerId(Long ownerId);

}
