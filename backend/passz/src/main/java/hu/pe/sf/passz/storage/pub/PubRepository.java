package hu.pe.sf.passz.storage.pub;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PubRepository extends JpaRepository<PubEntity, Long> {

    List<PubEntity> findAll();
}
