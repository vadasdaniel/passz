package hu.pe.sf.passz.restapi.controller.drink;

import hu.pe.sf.passz.restapi.controller.BaseController;
import hu.pe.sf.passz.storage.drink.DrinkEntity;
import hu.pe.sf.passz.storage.drink.DrinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("drink")
public class DrinkController extends BaseController {

    @Autowired
    DrinkRepository drinkRepository;

    @PostMapping("/create")
    private void create(@RequestBody DrinkEntity entity) {
        drinkRepository.save(entity);
    }

    @GetMapping("/list")
    private List<DrinkEntity> list(){
        return drinkRepository.findAll();
    }
}