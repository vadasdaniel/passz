package hu.pe.sf.passz.restapi.controller.user;

import hu.pe.sf.passz.restapi.controller.BaseController;
import hu.pe.sf.passz.storage.user.UserEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/friends")
public class FriendsController extends BaseController {

    @PostMapping("/add")
    private void addFriend(@RequestParam("id") Long id) {

    }

    @PostMapping("/addmore")
    private void addFriends(@RequestParam("ids") List<Long> ids) {
        UserEntity me = loggedInUser();

    }

}
