SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zip_code` int(10) NOT NULL,
  `longitude` double DEFAULT '0',
  `latitude` double DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `address` (`id`, `city`, `address`, `zip_code`, `longitude`, `latitude`) VALUES
(1,	'Veszprém',	'Ács u. 4',	8200,	47.0908647,	17.9015938),
(2,	'Veszprém',	'Kossuth Lajos u. 6',	8200,	47.0926046,	17.9099169),
(3,	'Veszprém',	'Mártírok útja 12',	8200,	47.0840291,	17.9127478);

DROP TABLE IF EXISTS `drink`;
CREATE TABLE `drink` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `drinkList`;
CREATE TABLE `drinkList` (
  `pub_id` bigint(20) DEFAULT NULL,
  `drink_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `felhasznalok`;
CREATE TABLE `felhasznalok` (
  `Email` text COLLATE utf8_hungarian_ci NOT NULL,
  `Jelszo` text COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

INSERT INTO `felhasznalok` (`Email`, `Jelszo`) VALUES
('admin@igyal.hu',	'admin');

DROP TABLE IF EXISTS `food`;
CREATE TABLE `food` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `foodList`;
CREATE TABLE `foodList` (
  `pub_id` bigint(20) DEFAULT NULL,
  `drink_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `pubs`;
CREATE TABLE `pubs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` bigint(20) NOT NULL,
  `rating` float DEFAULT '0',
  `opening` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `address` (`address`),
  CONSTRAINT `pubs_ibfk_1` FOREIGN KEY (`address`) REFERENCES `address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `pubs` (`id`, `name`, `address`, `rating`, `opening`) VALUES
(1,	'Gesztenyés Söröző',	1,	4.3,	12),
(2,	'Grand Canyon Söröző',	2,	4.3,	18),
(3,	'Sport Pub, Veszprém, Mártírok útja',	3,	3.7,	0);

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(40) NOT NULL,
  `lastname` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile_picture` varchar(40) DEFAULT '',
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(40) DEFAULT '',
  `last_position` varchar(100) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_email` (`email`),
  UNIQUE KEY `idx_phone` (`phone_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;